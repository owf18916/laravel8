<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = collect(['Laravel', 'PHP', 'HTML', 'CSS', 'Javascript']);
        
        $categories->each(function ($c) {
            Category::create([
                'name' => $c,
                'slug' => Str::slug($c) 
            ]);
        });
    }
}
