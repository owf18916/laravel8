<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Onne W Fajri',
            'username' => 'owf',
            'password' => bcrypt('Abcde123'),
            'email' => 'onne.wf@gmail.com'
        ]);
    }
}
