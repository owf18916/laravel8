<?php

namespace App\Http\Controllers;

use App\Models\{Post, Category, Tag};
use App\Http\Requests\PostRequest;
use Illuminate\Support\Str;

class PostController extends Controller
{
    public function index()
    {
        $posts = Post::latest()->paginate(6);
        return view('posts.index', compact('posts'));
    }

    public function create()
    {
        return view('posts.create', [
            'post' => new Post(),
            'categories' => Category::get(),
            'tags' => Tag::get()
        ]);
    }

    public function store(PostRequest $request)
    {
        $attr = $request->all();

        $attr['slug'] = Str::slug(request('title'));
        $attr['category_id'] = request('category');
        
        $post = Post::create($attr);

        $post->tags()->attach(request('tags'));

        session()->flash('success', 'Post was created.');

        return redirect('posts');
    }

    public function show(Post $post)
    {
        return view('posts.show', compact('post'));
    }

    public function edit(Post $post)
    {
        return view('posts.edit', [
            'post' => $post,
            'categories' => Category::get(),
            'tags' => Tag::get()
        ]);
    }

    public function update(PostRequest $request, Post $post)
    {   
        $attr = $request->all();
        $attr['category_id'] = $request->category;

        $post->update($attr);
        $post->tags()->sync(request('tags'));

        session()->flash('success', 'Post was updated.');

        return redirect('posts');
    }

    public function destroy(Post $post)
    {
        $post->tags()->detach();
        $post->delete();

        session()->flash('success', 'Post was deleted.');

        return redirect('posts');
    }
}
