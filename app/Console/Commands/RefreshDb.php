<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class RefreshDb extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'refresh:db';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Use to refresh & seed DB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->call('migrate:refresh');

        $this->call('db:seed');

        $this->info('Database has been refreshed and seeded.');
    }
}
