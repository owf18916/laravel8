<?php

use App\Http\Controllers\{
    HomeController,
    PostController,
    CategoryController,
    TagController
};

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Pages
Route::get('/', [HomeController::class, 'index'])->name('home');
Route::view('about', 'about');
Route::view('contact', 'contact');

// Posts
Route::get('posts', [PostController::class, 'index'])->name('posts.index');
Route::prefix('posts')->middleware('auth')->group( function() {
    Route::get('create', [PostController::class, 'create'])->name('posts.create');
    Route::post('create', [PostController::class, 'store']);
    Route::get('{post:slug}/edit', [PostController::class, 'edit']);
    Route::patch('{post:slug}/edit', [PostController::class, 'update']);
    Route::delete('{post:slug}/delete', [PostController::class, 'destroy']);
});
Route::get('posts/{post:slug}', [PostController::class, 'show']);

// Categories
Route::get('categories/{category:slug}', [CategoryController::class, 'show']);

// Tags
Route::get('tags/{tag:slug}', [TagController::class, 'show']);

Auth::routes();

