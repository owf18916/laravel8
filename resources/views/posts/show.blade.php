@extends('layouts.app', ['title' => $post->slug])

@section('title', 'The Posts')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md">
            <h1>{{ $post->title }}</h1>
            <small>
                <a href="/categories/{{ $post->category->slug }}">{{ $post->category->name }}</a>
                &middot; Published on {{ $post->created_at->format('d F, Y') }}
                &middot;
                @foreach ($post->tags as $tag)
                    <a href="/tags/{{ $tag->slug }}">{{ $tag->name }}</a>
                @endforeach
            </small>
            <hr>
            <p>{{ $post->body }}</p>

            @auth
                <button type="button" class="btn btn-link text-danger btn-sm p-0" data-toggle="modal" data-target="#exampleModal">
                    Delete
                </button>
            @endauth    
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Delete Post</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
          </div>
          <div class="modal-body">
            Are you sure to delete this post ?
            <p>Post title : {{ $post->title }}</p>
            <div class="text-secondary">
                <small>Published on {{ $post->created_at->format('d F, Y') }}</small>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
            <form action="/posts/{{ $post->slug }}/delete" method="post">
                @csrf
                @method('delete')
                <button type="submit" class="btn btn-danger">Delete</button>
            </form>
          </div>
        </div>
    </div>
</div>
@endsection