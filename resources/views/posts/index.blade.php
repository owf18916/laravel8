@extends('layouts.app', ['title' => 'The Posts'])

@section('content')
    <div class="container">
        <div class="d-flex justify-content-between">
            <div>
                @isset($category)
                    <h4>Category : {{ $category->name }}</h4>
                @endisset

                @isset($tag)
                    <h4>Tag : {{ $tag->name }}</h4>
                @endisset                    

                @if(!isset($category) && !isset($tag))
                     <h4>The Posts</h4>
                @endif                                
                <hr>
            </div>
            @auth
                <div>
                    <a href="/posts/create" class="btn btn-primary">New Post</a>
                </div>
            @endauth    
        </div>
        <div class="row">
            @forelse ($posts as $post)
                <div class="col-md-6">
                    <div class="card mb-3">
                        <div class="card-header">
                            {{ $post->title }}
                        </div>
                        <div class="card-body">
                            <div>
                                {{ Str::limit($post->body, 100, '.') }}
                            </div>

                            <a href="/posts/{{ $post->slug }}">Read more</a>
                        </div>
                        <div class="card-footer">
                            <div class="d-flex justify-content-between">
                                Posted on {{ $post->created_at->diffForHumans() }}
                                @auth
                                    <a href="/posts/{{ $post->slug }}/edit" class="btn btn-success">Edit</a>
                                @endauth
                            </div>
                        </div>
                    </div>
                </div>
            @empty
                <div class="col-md-6">
                    <div class="alert alert-info">There is no posts</div>
                </div>      
            @endforelse        
        </div>
        <div class="d-flex justify-content-center">
            {{ $posts->links() }}
        </div>
    </div>
@endsection